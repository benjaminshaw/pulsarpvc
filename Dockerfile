FROM python:3.7

RUN apt-get -y update && \
    apt-get -u update && \
    apt-get install -y vim

WORKDIR /home/pulsarpvc
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY *.py /home/pulsarpvc/
COPY pulsarpvc/ /home/pulsarpvc/pulsarpvc/
WORKDIR /home/pulsarpvc/fixtures
COPY fixtures/* /home/pulsarpvc/fixtures/
WORKDIR /home
RUN chmod 775 /home/pulsarpvc/*.py
ENV PATH="/home/pulsarpvc:${PATH}"
ENTRYPOINT /bin/bash
