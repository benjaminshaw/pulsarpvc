# PulsarPVC #

Tested with python3.7

A virtual environment is recommended. 

## Profile Variability ##

PulsarPVC is a toolset for measuring pulsar variability using the scikit-learn implemention of Gaussian Process Regression. The codebase is inspired by and uses much of the logic of Vpsr (see Brook et al., (2015)) and this paper should be consulted for details of how this technique works. Gaussian Processes for Machine Learning by Rasmussen and Williams is also a very useful text and is available as a free pdf online.

PulsarPVC has two main classes for studying pulse profile variability. These are 

* preprocess
* gppredict

Preprocess is a set of methods that preprocesses the input data and prepares a dataset to which we can apply a Gaussian Process. Preprocess takes a matrix of profiles. Each columns represents a single pulse profile so the number of columns is the number of observations we use to train a (set of) Gaussian process(es). Each column is structured as follows. The first n elements represent the power in each bin in a pulse profile, where n in the number of bins the profile is folded onto (1024 for Jodrell data taken after 2011). The second to last element is the integration time in seconds and the final element is the MJD on which the profile was recorded. Therefore there should be n + 2 rows and the length of each row is the total number of observations. It is important to note that the number of bins must be the same across all observations.

### Usage ###

Assuming pulsarPVC is in our python path, import the module

```
import pulsarpvc as pp
```

We instantiate our preprocessor as follows, passing in our matrix of raw data (test\_dataset\_1.txt).

```
preproc = pp.Preprocess("fixtures/test_dataset_1.txt")
```

We can extract the matrix of our raw data, if we want to and check the number of rows and columns

```
rawdata = preproc.rawdata
print(rawdata.shape[1])
print(rawdata.shape[0])
```

and we see that we have 1026 rows and 180 columns. So we have 180 observations and 1026 - 2 = 1024 bins in each profile. 

It may be that we've already been through each observation and checked that they are all good detections before we formed our input matrix. However, for ease, one is able to pass in a list of MJDs we know contain problematic observations but are still in the matrix. To exclude these from our data matrix we can use the filter method and pass in a list of MJDs we know are problematic. This list is just an ascii file which contains a list of those MJDs in a single column. 

filtered = preproc.filter("fixtures/badmjds\_1\_test\_dataset\_1.txt")

The filter method returns a copy of the rawdata matrix that doesn't contain the observation MJDs in badmjds\_1\_test\_dataset\_1.txt. 

```
filtered = preproc.filter("fixtures/badmjds_1_test_dataset_1.txt")
print(filtered.shape[0])
print(filtered.shape[1])
```

As it turns out we have two MJDs in our badmjds file and so our filtered data matrix now contains two fewer rows. We can now separate our MJD and integration time data from the profile data using the stripinput method. 

```
data, mjds, tobs, bins = preproc.stripinput(filtered)
```

This returns our data array (data) that just contains profile data, a list of observation MJDS (mjds), a list of integration times (tobs) and the number of bins in our profiles (bins). 

Now we use the debase method to remove any DC offsets from each profile. This is acheived by splitting each profile into octants, measuring the mean value in each and subtracting the lowest value from all bins in a given profile. debase also automatically removes "bad" profiles that were not including in the badmjds file. A profile is designated as "bad" if the standard deviation of the off-pulse region is greater than a factor of two larger than the median value taken from the off-pulse regions across all epochs. debase takes in our data array and returns the same array after debasing. It also returns a matrix of all of the profiles that have been removed, the off-pulse rms of the surviving profiles, the profile indices of the profiles that are removed and the profiles that are not. 

```
debased, removed_profiles, rms_per_epoch, outliers, inliers = preproc.debase(data)
```

Later on we'll need a list of MJDs that corresponds to the profile we want to keep. We can adjust our list of MJDs to account for the removed profiles. 

```
mjds = np.delete(mjds, outliers)
```

We then need to determine the index of the brightest pulse in our dataset as this profile will be used in the formation of a template profile from which we subtract all observations. This method takes in our data array and the off-pulse rms values for each profile in the data array. It does this by comparing the maximum flux in each profile with that profile's off-pulse rms. If the peak flux of the brightest profile is less than 20 sigma then an alert will be generated. In such cases one may wish to resample each profile to a lesser number of bins using the resample method (see code). 

index_of_brightest = preproc.find_brightest(debased, rms_per_epoch)	

The next step in preprocess is to form the template. This is acheived as follows

* The brightest profile is rotated so that the bin with the highest flux value is located at 1/4 of the phase (i.e. at the bin number corresponding to the total number of bins / 4).
* Each profile is then cross-correlated with the brightest to find how many bins it must be shifted by to align with the brightest. Each profile is then rotated to 1/4 of the phase. 
* For each bin, the median flux value for all epoch is computed. The resulting median values form our template profile. 

We use the align method to do this..

```
aligned, brightest, template = preproc.align(debased, index_of_brightest)
```
align takes in the debased data array and the index of the brightest profile. It returns

* A new array in which all profile are aligned to a common phase (1/4 of a turn). 
* An array corresponding to the brightest profile
* An array of median values corresponding to our template. 

We can plot the brightest and template using the plot_single method. These will be stored in the current working directory.

```
preproc.plot_single(brightest, "brightest")
preproc.plot_single(template, "template")
```

![Image Description](Brightest.jpg)

We can also plot all of the aligned profiles we're using. It's a good idea to do this as the debase method isn't perfect at identifying outliers and so it's sensible to manually inspect all of our good profiles to see if there are any that should be added to our badmjds list. 

```
preproc.plot(aligned, mjds, "aligned_profiles")
```

These will appear in a directory called aligned_profiles in the current working directory. If this directory already exists a warning will appear in the console. 

Next we can define what we consider to be the "on-pulse" region as this is the region of the profile that our Gaussian process will work on. For our example and using the brightest profile shown above we can set the on-pulse region as below.

```
start = 230
end = 280
```

In other words our on-pulse region comprises 50 bins between bin 230 and bin 280. Our final step is to extract data from our on-pulse region. This is effectively getting a version of our data-array that is zoomed in on the on-pulse region. This step also flags up profiles that might be unusual and should be inspected manually in order to determine if they should be excluded. This is left up to the user. This step does not removed any data. If just flags up profiles that it thinks might look a bit odd in comparison to the template. This final method is called get_on_pulse_data and is used as follows. 

```
zoomed_for_gp, bin_med, bin_std, off_pulse_std, zoomed_template  = preproc.get_on_pulse_data(aligned, template, mjds, start, end)
```

where..

* aligned is our data array
* template is the template profile
* mjds is a list of mjds corresponding to our "good" observations
* start is our starting bin number (i.e., the earlier bound of our on-pulse region)
* end is our ending bin number (i.e., the later bound of our on-pulse region)

The method returns in order..

* our data array zoomed into the on-pulse region. This is debased, aligned, normalised etc
* the median values for each profile (across all bins (full profile))
* the standard deviation values for each profile (across all bins (full profile))
* the off-pulse standard deviation across all off-pulse bins, across all epochs.
* The template, zoomed into the off-pulse region

This method plots the flagged profiles in a directory called flagged_profiles and the good profiles in a directory called good_profiles. The contents of these directories should be inspected manually to identify any errononeous profile features. 

Now we can instantiate our Gaussian process module.

```
predictor = pp.GpPredict(data=zoomed_for_gp, template=zoomed_template, off_pulse_std=off_pulse_std, start=start, end=end)
```

This assumes that the number of profile bins (in the unprocessed profiles) is 1024. If this is not the case the correct number of bins should be passed in using an additional parameter allbins (e.g., allbins=512). Next we form the profile residuals.

```
residuals = predictor.form_residuals()
``` 

This returns our zoomed-in data array but with the template subtracted. It is to this data that we will train our Gaussian processes. We do this by...

```
testmjds, inferred_array = predictor.predict(mjds=mjds, residuals=residuals, interval=1)
```

The interval is the frequency (in days^-1) at which we infer the value of the profile residuals. testmjds is an array of mjds corresponding to the epoch at which we make a prediction and the inferred_array is a data array of inferred profile residual values. A heatmap will be generated showing the profile variability. 
