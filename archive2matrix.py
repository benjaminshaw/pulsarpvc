#!/usr/bin/env python

import argparse
import logging
import os
import sys
import numpy as np

class MakeMatrix():

    def __init__(self, pulsar, src, nbins, freq):

        logging.basicConfig(format='[%(asctime)s][%(levelname)s][%(message)s]', level=logging.INFO)

        self.pulsar = pulsar
        self.src = src
        self.nbins = nbins
        self.freq = freq

    def run(self):
 
        rounding = int(np.floor(((self.freq-1)/100)))
        logging.info("Getting observations with {} MHz of {} MHz".format(rounding, self.freq))
        os.system('vap -c freq {}/* | grep -v filename > frequency_list.txt' .format(self.src,rounding,rounding+1,rounding-1))
        
        # Load in observations as array of (obsname, freq) tuple
        frequency_filtered = np.genfromtxt("frequency_list.txt", dtype=[('filename','S40'),('freq','S40')])

        # Number of observations near the frequency of interest
        nobs = frequency_filtered.shape[0]
        logging.info("Found {} observations near desired frequency".format(nobs))
        sorted_freq_filtered = []
        for i in range(nobs):
            sorted_freq_filtered.append(frequency_filtered['filename'][i])
        sorted_freq_filtered.sort(key=lambda sorted_freq_filtered: sorted_freq_filtered[1:7])
        logging.info("Sorted observations into date order")

        logging.info("Searching for observations with {} phase bins".format(self.nbins))
        rem_count = 0
        for i in range(nobs):
            epoch_name = sorted_freq_filtered[i].decode("utf-8")
            logging.info("Running pdv on epoch: {}".format(epoch_name))
            os.system("pdv -FTt {}/{} > temp.txt".format(self.src, epoch_name))
            stokes_line = np.genfromtxt('temp.txt', usecols=3, dtype=[('stokesI','float')], skip_header=1)
            if len(stokes_line) != self.nbins:
                logging.info("Excluding {}. Number of bins is {}".format(epoch_name, len(stokes_line)))
                rem_count+=1
        num_left = nobs - rem_count
        logging.info("Found {} observations with {} phase bins".format(num_left, self.nbins))

        logging.info("Preparing to extract powers")
        stokes_list = np.zeros((num_left, self.nbins))
        accepted = 0
        removed = 0
        for i in range(nobs):
            epoch_name = sorted_freq_filtered[i].decode("utf-8")
            logging.info("Running pdv on epoch: {}".format(epoch_name))
            os.system('pdv -FTt {}/{} > temp.txt '.format(self.src, epoch_name))
            stokes_line = np.genfromtxt('temp.txt', usecols=3, skip_header=1)
            if len(stokes_line) != self.nbins:
                logging.info("Excluding {}. Number of bins is {}".format(epoch_name, len(stokes_line)))
                removed+=1
            else:
                logging.info("Extracting observation duration for {}".format(epoch_name))
                os.system('vap -nc "mjd, length" {}/{} >> mjd.txt'.format(self.src, epoch_name))
                stokes_list[accepted] = stokes_line
                accepted+=1
        logging.info("Data extraction completed")

        logging.info("Preparing to create maxtrix")
        new_mjd = []
        mjdarray = np.genfromtxt("mjd.txt", dtype=[('filename2','S40'),('mjd','f8'),('len','f8')])
        for i in range(num_left):
            logging.info("Creating array for observation {}".format(i+1))
            new_mjd.append(mjdarray['mjd'][i])
        new_mjd.sort()
        logging.info("Completed")
        mjd_length = np.zeros((num_left, 2))
        for i in range(num_left):
            mjd_length [i,0] = mjdarray['mjd'][i]
            mjd_length [i,1] = mjdarray['len'][i]
        mjd_length_sorted = mjd_length[mjd_length[:,0].argsort()]
        new_length = mjd_length_sorted[:,1]
        new_mjd = mjd_length_sorted[:,0]

        logging.info("Preparing to create 2D matrix")
        stokes_columns = np.transpose(stokes_list)
        stokes_columns2 = np.zeros((self.nbins + 2, num_left))
        stokes_columns2 = np.vstack([stokes_columns, new_length, new_mjd])
        logging.info("Writing matrix to file")
        np.savetxt('{}_matrix_{}.txt'.format(self.pulsar, self.nbins), stokes_columns2, delimiter='\t')

        logging.info("Removing temporary files")
        try:
            os.remove('mjd.txt')
        except os.error:
            pass



def main():

    parser = argparse.ArgumentParser(description='Converter of scrunched archive files to power matrix')
    parser.add_argument('-p','--pulsar', help='Pulsar name', required=True, type=str)
    parser.add_argument('-s','--src', help='Directory containing scrunched archive files ONLY', required=True, type=str)
    parser.add_argument('-b','--phasebins', help='Number of phase bins in the observations', required=True, type=int)
    parser.add_argument('-f','--freq', help='Observation frequency (approx)', required=True, type=int)
    args = parser.parse_args()

    makematrix = MakeMatrix(args.pulsar, args.src, args.phasebins, args.freq)
    makematrix.run()

if __name__ == '__main__':
    main()
