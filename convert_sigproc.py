#!/usr/bin/env python

import argparse
import re
import sys

class Convert():

    def __init__(self, infile, nbins=512):
        self.infile = infile
        self.nbins = nbins

    def run(self):
        data = {}
        with open(self.infile, 'r') as sigproc:
            for line in sigproc.readlines():
                line = line.strip()
                if line.startswith('#'):
                    # Original #
                    #cols=re.search('([0-9]{3,})', line)
                    #mjd=cols.group(1)
                    ################################
                    # Updated
                    cols = line.split()
                    mjd = round((float(cols[1]) + (float(cols[2])/86400.0)), 5)
                    ################################
                    data[mjd]=[]
                else:
                    cols=re.search('[0-9][ ]{1,}([0-9E+-.]{1,})', line)
                    data[mjd].append(cols.group(1))
        this_bin = 0
        while this_bin < self.nbins:
            line=""
            for key in sorted(data.keys()):
                line = line + data[key][this_bin]+"\t"
            this_bin+=1
            print(line)

        line=""
        for key in sorted(data.keys()):
            line = line + "3000.000\t"
        print(line)

        line=""
        for key in sorted(data.keys()):
            line = line + str(key)+"\t"
        print(line)

def main():

    parser = argparse.ArgumentParser(description='Converts sigproc file into flux matrix')
    parser.add_argument('-f','--file', help='Input sigproc file', required=True, type=str)
    parser.add_argument('-n','--nbins', help='Number of bins per pulse period', required=True, type=int)
    args = parser.parse_args()

    convert = Convert(args.file, args.nbins)
    convert.run()

if __name__ == '__main__':
    main()
