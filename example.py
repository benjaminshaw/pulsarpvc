#!/usr/bin/env python

import pulsarpvc as pp
import numpy as np
import matplotlib.pyplot as plt
import sys

# First we create an intance of the preprocessor and pass in our data array
preproc = pp.Preprocess("fixtures/test_dataset_1.txt")
#preproc = pp.Preprocess("B1828-11_matrix_1024.txt", debug=True)

# This is the unfiltered data exactly as supplied
rawdata = preproc.rawdata

# We have a bad mjds file in fixtures/ 
# Lets use the filter() method to remove 
# those from the array
filtered = preproc.filter("fixtures/badmjds_1_test_dataset_1.txt")

# Lets now split the "data" part of the array from the metadata
data, mjds, tobs, bins = preproc.stripinput(filtered)

# Now we can plot the original profiles if we want
#preproc.plot(data, mjds, "original_profiles")

# Debase profiles
debased, removed_profiles, rms_per_epoch, outliers, inliers = preproc.debase(data)

# Which MJDs correspond to out outlier profiles?
outlier_mjds = np.delete(mjds, inliers)

# Adjust our MJDs list to account for the outliers
mjds = np.delete(mjds, outliers)

# Now we can plot the removed profiles if we want to
#preproc.plot(removed_profiles, outlier_mjds, "removed_profiles")

# Now we can find the index of the brightest profile 
index_of_brightest = preproc.find_brightest(debased, rms_per_epoch)

# We might want to resample our data is the brightest profile is not very bright. 
#resampled = preproc.resample(debased, 8)

# Align data with brightest and form template
aligned, brightest, template = preproc.align(debased, index_of_brightest)
preproc.plot_single(brightest, "brightest")
preproc.plot_single(template, "template")
#preproc.plot(aligned, mjds, "aligned_profiles")

start = 230
end = 280

# Select on pulse region
zoomed_for_gp, bin_med, bin_std, off_pulse_std, zoomed_template  = preproc.get_on_pulse_data(aligned, template, mjds, start, end)

# Instantiate predictor class
predictor = pp.GpPredict(data=zoomed_for_gp, template=zoomed_template, off_pulse_std=off_pulse_std, start=start, end=end, debug=False)

# Form profile residuals
residuals = predictor.form_residuals()

testmjds, inferred_array = predictor.predict(mjds=mjds, residuals=residuals, interval=1)
