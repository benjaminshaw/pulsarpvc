#!/usr/bin/env python

import argparse
import numpy as np

class Filter():

    def __init__(self, infile, lower, upper, tel):
        self.infile = infile
        self.tel = tel
        self.lower = lower
        self.upper = upper

    def run(self):
        with open(self.infile, 'r') as sigproc_data:
            for line in sigproc_data:
                fields = line.split()
                mjd1 = fields[0]
                date = fields[1]
                timex = fields[2]
                source = fields[3]
                tel = fields[4]
                freq = float(fields[5])
                mjd2 = fields[6]
                if tel != self.tel:
                    print(mjd1)
                if tel == self.tel:
                    if freq > self.upper:
                        print(mjd1)
                    if freq < self.lower:
                        print(mjd1)

def main():

    parser = argparse.ArgumentParser(description='Filters sigproc data file by telescope and frequency range')
    parser.add_argument('-f','--file', help='Input sigproc file', required=True, type=str)
    parser.add_argument('-t','--telescope', help='Telescope name (default=lovell)', required=False, type=str, default="lovell")
    parser.add_argument('-l','--lower', help='Lower frequency bound (MHz)', required=True, type=int)
    parser.add_argument('-u','--upper', help='Upper frequency bound (MHz)', required=True, type=int)
    args = parser.parse_args()

    filterer = Filter(args.file, args.lower, args.upper, args.telescope)
    filterer.run()

if __name__ == '__main__':
    main()
