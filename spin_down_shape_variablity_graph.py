import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable


#plot the nudot variation below the profile variation
off_pulse_std = np.loadtxt("off_pulse_std.dat")
data = np.loadtxt("mjd_nudot_err.dat")
inferred_array = np.loadtxt("inferred_array.dat")/off_pulse_std
mjds = np.loadtxt("shape_variabilty_mjds.dat")

mjd = data[:,0]
nudot = data[:,1]
nudot_err = data[:,2]

#find the first mjd for dfb in nudot data file
for i in range(len(mjd)):
    if int(round(mjd[i])) == int(round(mjds[0])):
        starting_value = i
    else:
        continue

mjd_new = mjd[starting_value:]
nudot_new = nudot[starting_value:]
nudot_err_new = nudot_err[starting_value:]

#plot figure
maxdifference = np.amax(inferred_array)
mindifference = np.amin(inferred_array)
limitdifference = np.max((maxdifference, np.abs(mindifference)))

fig, (ax0, ax1) = plt.subplots(2)
yaxis = []
ybins = inferred_array.shape[0]
yaxis.append(np.linspace(0, (290 - 230)/1024, 290 - 230))
yaxis = yaxis[0]
ylocs = np.linspace(0, ybins, 10)
yticklabels = []
for i in ylocs[:-1]:
    yticklabels.append(round(yaxis[int(i)], 2))
#ax0.set_yticks(ylocs)
#ax0.set_yticklabels( yticklabels)
im =ax0.imshow(inferred_array*1.0,
            aspect = "auto",
            
            cmap = "RdBu_r",
            interpolation='gaussian',
            vmin = -limitdifference,
            vmax = limitdifference,
            extent = [np.min(mjds), np.max(mjds), np.max(yticklabels), np.min(yticklabels)]
)
plt.setp(ax0.get_xticklabels(), visible=False)
#ax0.xaxis.set_visible(False)
"""
xbins = inferred_array.shape[1]
xlocs = np.arange(xbins, step=1000)
xticklabels = []

for i in xlocs:
    xticklabels.append(int(mjd_new[int(i)]))
ax0.set_xticks(xlocs)
ax0.set_xticklabels(xticklabels)
"""
#plt.tick_params(axis='both', which='major', labelsize=20)
#plt.ylim([max(ylocs), min(ylocs)])


ax1.errorbar(mjd_new,nudot_new,yerr = nudot_err_new)
ax1.set_xlim([np.min(mjds), np.max(mjds)])

plt.xlabel("Modified Julian Day", fontsize=15)
ax1.set_ylabel(r"$\nu$" +"\u0307" +" [1e-15 Hz/s]", fontsize = 15)
ax0.set_ylabel("Pulse phase", fontsize = 15)
divider = make_axes_locatable(ax0)
cax = divider.append_axes('top', size='5%', pad=0.05)
fig.colorbar(im, cax=cax, orientation='horizontal')
plt.subplots_adjust(hspace=.0)
ax0.grid()
#ax0.minorticks_on()
#ax1.minorticks_on()
ax1.grid()
plt.savefig("Double_Plot")

