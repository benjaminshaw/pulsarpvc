# Simple tests for predictgp
import os
from pytest import mark
import numpy as np
import sys
import pulsarpvc

DATA_DIR = os.path.dirname(__file__) + "/fixtures"

def get_obj():
    preproc = pulsarpvc.Preprocess(DATA_DIR + "/test_dataset_1.txt")
    return preproc

@mark.xfail(reason="Fixture needs redefining after bug fix")
def test_remove_badmjds_that_exist():
    badmjdslist = np.loadtxt(DATA_DIR + "/badmjds_1_test_dataset_1.txt", unpack=True)
    bad_number = len(badmjdslist)
    unfiltered = get_obj().rawdata
    filtered = get_obj().filter(DATA_DIR + "/badmjds_1_test_dataset_1.txt")
    unfiltered_number = unfiltered.shape[1]
    filtered_number = filtered.shape[1]
    difference = unfiltered_number - filtered_number
    assert difference == bad_number

def test_remove_badmjds_that_dont_exist():
    unfiltered = get_obj().rawdata
    filtered = get_obj().filter(DATA_DIR + "/badmjds_2_test_dataset_1.txt")
    unfiltered_number = unfiltered.shape[1]
    filtered_number = filtered.shape[1]
    assert unfiltered_number == filtered_number

def test_input_stripper():
    preproc = pulsarpvc.Preprocess(DATA_DIR + "/test_dataset_1.txt")
    filtered = preproc.filter(DATA_DIR + "/badmjds_1_test_dataset_1.txt")
    data, mjds, tobs, bins = preproc.stripinput(filtered) 
    assert len(mjds) == filtered.shape[1]
    assert len(tobs) == filtered.shape[1]
    assert bins == filtered.shape[0]-2
    assert data.shape[0] == filtered.shape[0]-2

@mark.xfail(reason="Input array has changed due to bug fix")
def test_outlier_removal():
    preproc = pulsarpvc.Preprocess(DATA_DIR + "/test_dataset_1.txt")
    filtered = preproc.filter(DATA_DIR + "/badmjds_1_test_dataset_1.txt")
    data, mjds, tobs, bins = preproc.stripinput(filtered)
    debased, removed_profiles, rms_removed, outliers, inliers = preproc.debase(data)
    assert len(outliers) == 54
    assert len(inliers) == 124
    assert len(rms_removed) == 124

def test_find_brightest():
    preproc = pulsarpvc.Preprocess(DATA_DIR + "/test_dataset_1.txt")
    filtered = preproc.filter(DATA_DIR + "/badmjds_1_test_dataset_1.txt")
    data, mjds, tobs, bins = preproc.stripinput(filtered)
    debased, removed_profiles, rms_data, outliers, inliers = preproc.debase(data)
    brightest_index = preproc.find_brightest(debased, rms_data)
    assert brightest_index == 46

def test_resampler():
    resample_factor = 8
    preproc = pulsarpvc.Preprocess(DATA_DIR + "/test_dataset_1.txt")
    filtered = preproc.filter(DATA_DIR + "/badmjds_1_test_dataset_1.txt")
    data, mjds, tobs, bins = preproc.stripinput(filtered)
    debased, removed_profiles, rms_data, outliers, inliers = preproc.debase(data)
    samples = debased.shape[0]
    resampled = preproc.resample(debased, resample_factor)
    assert resampled.shape[0] == int(samples/resample_factor)

@mark.xfail(reason="Known error to be fixed")
def test_aligner():
    preproc = pulsarpvc.Preprocess(DATA_DIR + "/test_dataset_1.txt")
    filtered = preproc.filter(DATA_DIR + "/badmjds_1_test_dataset_1.txt")
    data, mjds, tobs, bins = preproc.stripinput(filtered)
    debased, removed_profiles, rms_data, outliers, inliers = preproc.debase(data)
    brightest_index = preproc.find_brightest(debased, rms_data)
    aligned, brightest, template = preproc.align(debased, brightest_index)
    expected_template = np.loadtxt(DATA_DIR + "/template_dataset_1.txt")
    assert len(template) == len(expected_template)
    for i in range(0, len(template)):
        assert template[i] == expected_template[i]
   # Need to add test for aligned 

def test_array_construction():
    start = 200
    end = 350
    preproc = pulsarpvc.Preprocess(DATA_DIR + "/test_dataset_1.txt")
    filtered = preproc.filter(DATA_DIR + "/badmjds_1_test_dataset_1.txt")
    data, mjds, tobs, bins = preproc.stripinput(filtered)
    debased, removed_profiles, rms_data, outliers, inliers = preproc.debase(data)
    mjds = np.delete(mjds, outliers)
    brightest_index = preproc.find_brightest(debased, rms_data)
    aligned, brightest, template = preproc.align(debased, brightest_index)
    zoomed_aligned_normed = preproc.get_on_pulse_data(aligned, template, mjds, start, end)
    # Finish test

