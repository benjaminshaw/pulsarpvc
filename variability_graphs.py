import pulsarpvc as pp
import numpy as np
import matplotlib.pyplot as plt
import argparse
from mpl_toolkits.axes_grid1 import make_axes_locatable


class nudot_variability_plot():
	
	def __init__(self, inferred_array, mjd_nudot_err, variability_mjd, off_peak_std, start, end, bins):

		#load data from files or arrays
		
		if isinstance(inferred_array, str):
			self.inferred_array = np.loadtxt(inferred_array)
		elif isinstance(inferred_array, np.ndarray):
			self.inferred_array = inferred_array
		else: raise Exception("input error")
		
		if isinstance(mjd_nudot_err, str):
			self.mjd_nudot_err = np.loadtxt(mjd_nudot_err)
		elif isinstance(mjd_nudot_err, np.ndarray):
			self.mjd_nudot_err = mjd_nudot_err
		else: raise Exception("input error")
		
		if isinstance(variability_mjd, str):
			self.variability_mjd = np.loadtxt(variability_mjd)
		elif isinstance(variability_mjd, np.ndarray):
			self.variability_mjd = variability_mjd
		else: raise Exception("input error")

		self.off_peak_std = off_peak_std
		self.start = start
		self.end = end
		self.bins = bins
		
		#split the mjd_nudot_err
		self.mjd = self.mjd_nudot_err[:,0]
		self.nudot =  self.mjd_nudot_err[:,1]
		self.err  = self.mjd_nudot_err[:,2]
		

	def cut_data(self, mjd, nudot, err, variability_mjd):
	
		#cuts data to correct length
		
		#find the min and the max mjd to correspond to both mjd arrays
		diff_min = np.abs(mjd - np.min(variability_mjd))
		first_mjd_arg = np.argmin(diff_min)

		diff_max = np.abs(mjd - np.max(variability_mjd))
		last_mjd_arg = np.argmin(diff_max)

		#slices the data to the min and the max mjd
		cut_mjd = mjd[first_mjd_arg:(last_mjd_arg+1)] 
		cut_nudot = nudot[first_mjd_arg:(last_mjd_arg+1)]
		cut_err = err[first_mjd_arg:(last_mjd_arg+1)]
		
		return [cut_mjd, cut_nudot, cut_err]
		
	def plot(self, cut_mjd, cut_nudot, cut_err, inferred_array, variability_mjd, off_peak_std, start, end, bins):
	
		#plot graphs
		
		fig, (ax_1, ax_2) = plt.subplots(2)
	
		inferred_array = np.true_divide(inferred_array,off_peak_std) #changes to units of noise std
		for i in range(0, len(variability_mjd)):
			ax_1.axvline((variability_mjd[i]), ymin=0.01, ymax=0.05, linestyle='solid', color='k', linewidth=1)

		#add in y ticks for the pulse phase
		yaxis = []
		ybins = inferred_array.shape[0] 
		yaxis.append(np.linspace(0, (end - start)/bins, end - start))
		yaxis = yaxis[0]
		ylocs = np.linspace(0, ybins, 10)

		yticklabels = []
		for i in ylocs[:-1]:
			yticklabels.append(round(yaxis[int(i)], 2))

		max_diff =  np.amax(inferred_array)
		min_diff = np.amin(inferred_array)

		lim_diff = np.max((max_diff, np.abs(min_diff)))

		#plot shape variability map
		im = ax_1.imshow(inferred_array,
		extent = [ np.min(variability_mjd), np.max(variability_mjd),np.max(yticklabels), np.min(yticklabels)], 
		vmin = -lim_diff, 
		vmax = lim_diff, 
		aspect='auto', 
		cmap ="RdBu_r", 
		interpolation = "gaussian")

		ax_2.errorbar(cut_mjd, cut_nudot, yerr = cut_err)
		ax_2.set_xlim(np.min(variability_mjd),np.max(variability_mjd))
		plt.xlabel("Modified Julian Day", fontsize =15)
		plt.setp(ax_1.get_xticklabels(), visible=False)
		ax_2.set_ylabel(r"$\nu$"+"\u0307"+" [1e-15 Hz/s]", fontsize = 15)
		ax_1.set_ylabel("Pulse Phase", fontsize = 15)
		divider = make_axes_locatable(ax_1)
		cax = divider.append_axes("top",size = "5%", pad = 0.05)
		fig.colorbar(im,cax=cax, orientation = "horizontal")
		cax.xaxis.set_ticks_position("top")
		plt.subplots_adjust(hspace=.0)
		ax_1.minorticks_on()
		ax_2.minorticks_on()
		ax_1.xaxis.grid(which = 'minor', color='#DDDDDD')
		ax_1.xaxis.grid(which = 'major', color='#DDDDDD')
		ax_2.xaxis.grid(which = 'minor', color='#DDDDDD')
		ax_2.xaxis.grid(which = 'major', color='#DDDDDD')
		ax_1.get_yaxis().set_label_coords(-0.1,0.5)
		ax_2.get_yaxis().set_label_coords(-0.1,0.5)
		for i in range(0, len(variability_mjd)):
			ax_1.axvline((variability_mjd[i]), ymin=0.01, ymax=0.05, linestyle='solid', color='k', linewidth=1)

		plt.savefig("variability_graphs_comparison.png")
		fig.show()

	def run(self):
	
		[cut_mjd, cut_nudot, cut_err] = self.cut_data(self.mjd, self.nudot, self.err, self.variability_mjd)
		self.plot(cut_mjd,cut_nudot,cut_err,self.inferred_array, self.variability_mjd, self.off_peak_std, self.start, self.end,self.bins)

def main():

	parser = argparse.ArgumentParser(description='Plots variability map above nudot graph')
	parser.add_argument('-i','--inferred_array', help='inferred array dat file', required=True)
	parser.add_argument('-n','--nudot', help='File of nudot [mjd, nudot, err]', required=True)
	parser.add_argument('-m', '--variability_mjd', help="List of mjds used by the variability map", required = True)
	parser.add_argument('-o', '--off_peak_std', help="off peak standard deviation", type = float, required = True)
	parser.add_argument('-s', '--start', help = "bin number of the start of the peak fit",type = int, required = True)
	parser.add_argument('-e', '--end', help = "bin numbers of the end of the peak", type = int, required = True)
	parser.add_argument('-b', '--bins', help = "optional: total number of bins in pulse shape (default 1024)", type = int, default = 1024)
	
	args = parser.parse_args()
	
	DoAThing = nudot_variability_plot(args.inferred_array, args.nudot, args.variability_mjd, args.off_peak_std, args.start, args.end, args.bins)
	DoAThing.run()
   
if __name__ == '__main__':
    main()

